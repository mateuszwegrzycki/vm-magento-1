Black        0;30     Dark Gray     1;30
Red          0;31     Light Red     1;31
Green        0;32     Light Green   1;32
Brown/Orange 0;33     Yellow        1;33
Blue         0;34     Light Blue    1;34
Purple       0;35     Light Purple  1;35
Cyan         0;36     Light Cyan    1;36
Light Gray   0;37     White         1;37
And then use them like this in your script:

#    .---------- constant part!
#    vvvv vvvv-- the code from above
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

#Install Apache
echo -e "${GREEN}##### Install Apache #####${NC}"
sudo apt-get update
sudo apt-get install apache2 -y
sudo ufw allow 'Apache'
sudo a2enmod rewrite
sudo sed -i "182i <Directory /var/www/html/>\n\tAllowOverride All\n\tRequire all granted\n</Directory> " /etc/apache2/apache2.conf

#Remove all PHP versions if exists
echo -e "${GREEN}##### Remove all PHP versions#####${NC}"
sudo apt-get purge `dpkg -l | grep php| awk '{print $2}' |tr "\n" " "` -y
#sudo apt-get purge 'php*' -y

#Install PHP 5.6
echo -e "${GREEN}##### Install PHP #####${NC}"
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php5.6

#Install PHP extensions
echo -e "${GREEN}##### Install PHP extensions #####${NC}"
sudo apt-get install php5.6-simplexml -y
sudo apt-get install php5.6-mysql -y
sudo apt-get install php5.6-curl -y
sudo apt-get install php5.6-dom -y
sudo apt-get install php5.6-gd -y
sudo apt-get install php5.6-iconv -y
sudo apt-get install php5.6-mcrypt -y
sudo apt-get install php5.6-pdo -y

#Install MySQL
echo -e "${GREEN}##### Install MySQL #####${NC}"
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password qwerty123'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password qwerty123'
sudo apt-get -y install mysql-server-5.7
sudo sed -i 's/= 127.0.0.1/= 0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf
#for mysql version lower than 5.7
#sudo sed -i 's/= 127.0.0.1/= 0.0.0.0/g' /etc/mysql/my.cnf

mysql -u root -pqwerty123 -e "CREATE USER 'magento'@'%' IDENTIFIED BY 'magento'"
mysql -u root -pqwerty123 -e "GRANT ALL PRIVILEGES ON *.* TO 'magento'@'%' WITH GRANT OPTION"
mysql -u root -pqwerty123 -e "FLUSH PRIVILEGES"
mysql -u magento -pmagento -e "CREATE DATABASE magento"

sudo /etc/init.d/mysql restart
sudo service apache2 restart

#Enable password authentication for SSH
echo -e "${GREEN}##### #Enable password authentication for SSH #####${NC}"
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo systemctl restart sshd


sudo apt-get update
sudo apt-get upgrade

#Install composer
echo -e "${GREEN}##### Install Composer #####${NC}"
sudo apt-get install php5.6-curl php5.6-cli php5.6-mbstring git unzip -y
cd ~
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"

echo -e "${GREEN}##### Change /var/www/html/ directory's permissions #####${NC}"
sudo rm -R /var/www/html/
sudo mkdir /var/www/html/
cd /var/www/html/
sudo chown -R :www-data .
sudo usermod -a -G www-data vagrant
sudo service apache2 restart

#Download Magento 1
echo -e "${GREEN}##### Download Magento 1 #####${NC}"
cd /var/www/html/
git clone https://github.com/OpenMage/magento-mirror.git .

#Install Magento 1
echo -e "${GREEN}##### Install Magento 1 #####${NC}"
php install.php \
--license_agreement_accepted yes \
--locale en_US \
--timezone "Europe/Warsaw" \
--default_currency USD \
--db_host 127.0.0.1 \
--db_name magento \
--db_user magento \
--db_pass magento \
--url "magento1-dev.test" \
--use_rewrites yes \
--use_secure no \
--secure_base_url "magento1-dev.test" \
--use_secure_admin no \
--admin_firstname Magento \
--admin_lastname Admin \
--admin_email "admin@example.com" \
--admin_username admin \
--admin_password qwerty123 \
--skip_url_validation \
--session_save db

#Instal magerun
echo -e "${GREEN}##### Install magerun #####${NC}"
cd /var/www/html/
wget https://files.magerun.net/n98-magerun.phar
shasum -a256 n98-magerun.phar
chmod +x ./n98-magerun.phar