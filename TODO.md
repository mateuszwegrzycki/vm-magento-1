[ ] change mysql binding from 127.0.0.1 to 0.0.0.0 (it is different path fot mysql 5.6 and 5.7)
[ ] change chown for html from . to -R
[ ] change host port - use port with the same prefix e.g. 80 redirect to 3280, 3306 to 323306
[ ] add interactive menu to vagrant vm installation
[ ] add Xdebug installation